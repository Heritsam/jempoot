import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';

class ContactPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: orangeColor),
        centerTitle: true,
        title: Text(
          'Contact Us',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'PUNYA PERTANYAAN? SARAN? ATAU MASUKAN?',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
            ),
            SizedBox(height: 8),
            Text(
              'Silahkan hubungi kami ya!',
              style: TextStyle(fontSize: 16, color: darkGreyColor),
            ),
            SizedBox(height: 24),
            Row(
              children: [
                Icon(Icons.phone, color: orangeColor),
                SizedBox(width: 16),
                Text(
                  '0812 3456 7890',
                  style: TextStyle(fontSize: 16),
                ).expanded(),
                SizedBox(width: 16),
                InkResponse(
                  onTap: () {},
                  child: Text('Copy', style: TextStyle(color: orangeColor)),
                ),
              ],
            )
                .padding(
                  horizontal: 16,
                  vertical: 12,
                )
                .decorated(
                  color: lightGreyColor,
                  borderRadius: BorderRadius.circular(defaultBorderRadius),
                ),
            SizedBox(height: 16),
            Row(
              children: [
                Icon(Icons.mail_rounded, color: orangeColor),
                SizedBox(width: 16),
                Text(
                  'hello@mrjempoot.com',
                  style: TextStyle(fontSize: 16),
                ).expanded(),
                SizedBox(width: 16),
                InkResponse(
                  onTap: () {},
                  child: Text('Copy', style: TextStyle(color: orangeColor)),
                ),
              ],
            )
                .padding(
                  horizontal: 16,
                  vertical: 12,
                )
                .decorated(
                  color: lightGreyColor,
                  borderRadius: BorderRadius.circular(defaultBorderRadius),
                ),
          ],
        ),
      )
          .constrained(
            height: mediaQuery.size.height,
          )
          .backgroundImage(
            DecorationImage(
              image: AssetImage('assets/bg_kyuubi.png'),
              fit: BoxFit.cover,
            ),
          ),
    );
  }
}
