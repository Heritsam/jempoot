import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/buttons/circle_button.dart';
import '../../widgets/buttons/rounded_button.dart';

class OtpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Stack(
      children: [
        Container(
          height: mediaQuery.size.height,
          width: mediaQuery.size.width,
          decoration: BoxDecoration(
            color: Colors.white70,
            image: DecorationImage(
              image: AssetImage('assets/bg_courier.png'),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(Colors.white70, BlendMode.lighten),
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(16),
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 16),
                Text(
                  'MASUKKAN KODE VERIFIKASI YANG TELAH KAMI KIRIMKAN!',
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 24,
                    color: orangeColor,
                  ),
                ),
                SizedBox(height: 8),
                Text('Kode tersebut berupa SMS berisi 4 digit angka'),
                SizedBox(height: 80),
                PinCodeTextField(
                  appContext: context,
                  mainAxisAlignment: MainAxisAlignment.center,
                  length: 4,
                  onChanged: (value) {},
                  enableActiveFill: true,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  keyboardType: TextInputType.number,
                  pinTheme: PinTheme(
                    inactiveFillColor: Colors.white,
                    activeFillColor: Colors.white,
                    activeColor: Colors.white,
                    inactiveColor: Colors.white,
                    selectedColor: Colors.white,
                    selectedFillColor: Colors.white,
                    borderWidth: 0,
                    borderRadius: BorderRadius.circular(defaultBorderRadius),
                    shape: PinCodeFieldShape.box,
                    fieldOuterPadding: const EdgeInsets.symmetric(horizontal: 16),
                  ),
                  textStyle: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                  boxShadows: [
                    BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0, 10),
                      spreadRadius: -5,
                      blurRadius: 8,
                    ),
                  ],
                ),
                SizedBox(height: 24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Kirim ulang',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Text(
                      ' dalam ',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(
                      '45',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 80),
                RoundedButton(
                  onPressed: () {
                    Navigator.popUntil(context, ModalRoute.withName('/login'));
                    Navigator.pushReplacementNamed(context, '/home');
                  },
                  label: 'Masuk',
                  horizontalPadding: 48,
                ).center(),
                SizedBox(height: 128),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircleButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.arrow_back, color: Colors.white),
                    ),
                    SizedBox(height: 12),
                    Text('Ganti nomor',
                        style: TextStyle(color: Colors.black54)),
                  ],
                ).center(),
                SizedBox(height: 24),
              ],
            ).safeArea(),
          ),
        ),
      ],
    );
  }
}
