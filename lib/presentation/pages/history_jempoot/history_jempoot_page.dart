import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/buttons/circle_button.dart';
import '../../widgets/cards/jempoot_card_alt.dart';

class HistoryJempootPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: orangeColor),
        centerTitle: true,
        title: Text(
          'Jempoot',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: ListView.builder(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        itemCount: 3,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: JempootCardAlt(
              onTap: () {
                Navigator.pushNamed(context, '/history-jempoot-detail');
              },
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black12, width: 1.2),
                borderRadius: BorderRadius.circular(defaultBorderRadius),
              ),
              child: Row(
                children: [
                  CircleButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/history-jempoot-detail');
                    },
                    child: SvgPicture.asset('assets/ic_jempoot.svg'),
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(width: 16),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Perjalanan ke Jl. Petir no. 25',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: 4),
                      Text(
                        'Senin, 12/12/2021',
                        style: TextStyle(fontSize: 12, color: orangeColor),
                      ),
                    ],
                  ).expanded(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
