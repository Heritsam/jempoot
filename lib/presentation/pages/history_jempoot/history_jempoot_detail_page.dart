import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:styled_widget/styled_widget.dart';
import 'package:timeline_tile/timeline_tile.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/circle_image.dart';
import '../../widgets/rating_stars.dart';

class HistoryJempootDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: orangeColor),
        centerTitle: true,
        title: Text(
          'Jempoot',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Kode pemesanan: FD-98JM918JQO82',
              style: TextStyle(color: darkGreyColor),
            )
                .center()
                .padding(
                  vertical: 4,
                )
                .decorated(
                  color: lightGreyColor,
                  borderRadius: BorderRadius.circular(defaultBorderRadius),
                ),
            SizedBox(height: 32),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '6 km - 1 jam 20 menit ',
                  style: TextStyle(
                    color: orangeColor,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  '- Tiba sekitar pukul 12:20 WIB',
                  style: TextStyle(color: darkGreyColor),
                ),
              ],
            ),
            SizedBox(height: 16),
            Container(
              height: 180,
              width: mediaQuery.size.width,
              decoration: BoxDecoration(
                color: lightGreyColor,
                borderRadius: BorderRadius.circular(defaultBorderRadius),
              ),
              child: const GoogleMap(
                initialCameraPosition: CameraPosition(
                  target: LatLng(-6.5776978, 106.7792039),
                  zoom: 14,
                ),
                myLocationButtonEnabled: false,
                scrollGesturesEnabled: false,
                zoomGesturesEnabled: false,
              ).clipRRect(all: defaultBorderRadius),
            ).boxShadow(
              color: Colors.black26,
              offset: Offset(0, 20),
              spreadRadius: -15,
              blurRadius: 12,
            ),
            SizedBox(height: 16),
            TimelineTile(
              afterLineStyle: LineStyle(thickness: 2, color: darkGreyColor),
              isFirst: true,
              indicatorStyle: IndicatorStyle(
                indicator: Image.asset('assets/ic_current_location.png'),
                height: 32,
                width: 32,
              ),
              endChild: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Rumah',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Text(
                    'Jl. Cibereum Petir no. 25, Bogor',
                    style: TextStyle(color: darkGreyColor),
                  ),
                ],
              ).padding(horizontal: 16, vertical: 12),
            ),
            TimelineTile(
              beforeLineStyle: LineStyle(thickness: 2, color: darkGreyColor),
              isLast: true,
              indicatorStyle: IndicatorStyle(
                indicator: Image.asset('assets/ic_destination.png'),
                height: 32,
                width: 32,
              ),
              endChild: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Kantor',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Text(
                    'Jl. Babakan Madang no. 12, Bogor',
                    style: TextStyle(color: darkGreyColor),
                  ),
                ],
              ).padding(horizontal: 16, vertical: 12),
            ),
            SizedBox(height: 24),
            Row(
              children: [
                CircleImage(size: 50).elevation(
                  4,
                  borderRadius: BorderRadius.circular(50),
                ),
                SizedBox(width: 16),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Mas Dav (driver)',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: greenColor,
                      ),
                    ),
                    SizedBox(height: 2),
                    Text(
                      'F 1234 AB',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: darkGreyColor,
                      ),
                    ),
                  ],
                ).expanded(),
              ],
            ),
            SizedBox(height: 32),
            Row(
              children: [
                RatingStars(voteAverage: 4, starSize: 14)
                    .padding(horizontal: 10, vertical: 4)
                    .decorated(
                      color: lightGreyColor,
                      borderRadius: BorderRadius.circular(50),
                    ),
              ],
            ),
            SizedBox(height: 16),
            Text(
              'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
              style: TextStyle(
                color: darkGreyColor,
                fontSize: 12,
                height: 1.5,
              ),
            )
                .padding(
                  horizontal: 16,
                  vertical: 12,
                )
                .decorated(
                  border: Border.all(color: mediumGreyColor),
                  borderRadius: BorderRadius.circular(defaultBorderRadius),
                ),
            SizedBox(height: 32),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total Pembayaran',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Rp 234.000',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: darkGreyColor,
                        decoration: TextDecoration.lineThrough,
                      ),
                    ),
                    Text(
                      'Rp 123.000',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: greenColor,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 64),
          ],
        ),
      ),
    );
  }

  Widget _buildRow({
    required String title,
    required int price,
    Color priceColor = Colors.black,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title, style: TextStyle(color: darkGreyColor)),
        Text(
          'Rp $price',
          style: TextStyle(fontWeight: FontWeight.w500, color: priceColor),
          textAlign: TextAlign.right,
        ),
      ],
    ).padding(bottom: 8);
  }
}
