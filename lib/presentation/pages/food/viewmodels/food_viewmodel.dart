import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final foodViewModel = ChangeNotifierProvider<FoodViewModel>((ref) {
  return FoodViewModel(ref.read);
});

class FoodViewModel extends ChangeNotifier {
  final Reader _read;

  FoodViewModel(this._read);
}
