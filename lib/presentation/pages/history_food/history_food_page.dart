import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/cards/jempoot_card_alt.dart';
import '../../widgets/circle_image.dart';

class HistoryFoodPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: orangeColor),
        centerTitle: true,
        title: Text(
          'Food',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: ListView.builder(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        itemCount: 3,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: JempootCardAlt(
              onTap: () {
                Navigator.pushNamed(context, '/history-food-detail');
              },
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black12, width: 1.2),
                borderRadius: BorderRadius.circular(defaultBorderRadius),
              ),
              child: Row(
                children: [
                  Container(
                    height: 120,
                    width: 80,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(defaultBorderRadius),
                      image: DecorationImage(
                        image: AssetImage('assets/resto_4.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(width: 16),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'KFC - Pajajaran Bogor',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: 4),
                      Text(
                        'Senin, 12/12/2021',
                        style: TextStyle(fontSize: 12, color: orangeColor),
                      ),
                      SizedBox(height: 12),
                      Row(
                        children: [
                          CircleImage(size: 18),
                          SizedBox(width: 8),
                          Text(
                            'Mas Dav (driver)',
                            style: TextStyle(fontSize: 12),
                          ),
                        ],
                      ),
                      SizedBox(height: 12),
                      Text(
                        'Rp 200.000',
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          color: darkGreyColor,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ),
                      Text(
                        'Rp 121.350',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: greenColor,
                        ),
                      ),
                    ],
                  ).expanded(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
