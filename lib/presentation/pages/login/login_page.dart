import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/buttons/circle_button.dart';
import '../../widgets/buttons/rounded_button.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Stack(
        children: [
          Container(
            height: mediaQuery.size.height,
            width: mediaQuery.size.width,
            decoration: BoxDecoration(
              color: Colors.white70,
              image: DecorationImage(
                image: AssetImage('assets/bg_delivery.png'),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(Colors.white70, BlendMode.lighten),
              ),
            ),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            body: SingleChildScrollView(
              padding: const EdgeInsets.all(16),
              physics: BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 16),
                  Text(
                    'SUDAH SIAP DENGAN JASA KAMI?',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 24,
                      color: orangeColor,
                    ),
                  ),
                  SizedBox(height: 8),
                  Text('Yuk gabung dulu...'),
                  SizedBox(height: 64),
                  SvgPicture.asset('assets/logo.svg').center(),
                  SizedBox(height: 64),
                  Row(
                    children: [
                      Text(
                        '+62',
                        style: TextStyle(fontSize: 14, color: Colors.black54),
                      ).padding(horizontal: 12),
                      Container(height: 31, width: 1, color: mediumGreyColor),
                      TextField(
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'No. Handphone',
                          hintStyle:
                              TextStyle(fontSize: 14, color: Colors.black54),
                          contentPadding:
                              const EdgeInsets.symmetric(horizontal: 12),
                        ),
                        style: TextStyle(fontSize: 14),
                        keyboardType: TextInputType.number,
                        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      ).expanded(),
                    ],
                  ).decorated(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(defaultBorderRadius),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 18),
                        spreadRadius: -15,
                        blurRadius: 8,
                      ),
                    ],
                  ),
                  SizedBox(height: 24),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      RoundedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/otp');
                        },
                        label: 'Minta Kode',
                      ),
                    ],
                  ),
                  SizedBox(height: 128),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircleButton(
                        onPressed: () {
                          Navigator.pushReplacementNamed(context, '/home');
                        },
                        child: Icon(Icons.arrow_forward, color: Colors.white),
                      ),
                      SizedBox(height: 12),
                      Text('Lewati', style: TextStyle(color: Colors.black54)),
                    ],
                  ).center(),
                  SizedBox(height: 24),
                ],
              ).safeArea(),
            ),
          ),
        ],
      ),
    );
  }
}
