import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import '../../widgets/chips/small_chip.dart';
import 'widgets/modal_food_detail.dart';

class FoodRestoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: orangeColor),
        centerTitle: false,
        backgroundColor: Colors.white,
        title: Text(
          'Jl. Cibereum Petir no. 25',
          style: TextStyle(
            fontSize: 14,
            color: darkGreyColor,
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.favorite, color: mediumGreyColor),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/food-checkout');
        },
        child: SvgPicture.asset('assets/ic_cart.svg', height: 38, width: 38),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 125,
              width: mediaQuery.size.width,
              decoration: BoxDecoration(
                color: mediumGreyColor,
                borderRadius: BorderRadius.circular(defaultBorderRadius),
                image: DecorationImage(
                  image: AssetImage('assets/resto_1.png'),
                  fit: BoxFit.cover,
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 20),
                    spreadRadius: -15,
                    blurRadius: 12,
                  ),
                ],
              ),
            ),
            SizedBox(height: 16),
            Text(
              'Burger Mas Dav - Jl. Cilok',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: 8),
            ListView(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              physics: BouncingScrollPhysics(),
              children: [
                SmallChip(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.star, size: 14, color: Colors.amber),
                      SizedBox(width: 2),
                      Text(
                        '4.0',
                        style: TextStyle(fontSize: 11),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 8),
                SmallChip(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.location_on,
                        size: 14,
                        color: darkGreyColor,
                      ),
                      SizedBox(width: 2),
                      Text(
                        '1 km',
                        style: TextStyle(fontSize: 11),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 8),
                SmallChip(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.watch_later,
                        size: 14,
                        color: darkGreyColor,
                      ),
                      SizedBox(width: 2),
                      Text(
                        '30 min',
                        style: TextStyle(fontSize: 11),
                      ),
                    ],
                  ),
                ),
              ],
            ).constrained(
              height: 20,
              width: mediaQuery.size.width - 120 - 48,
            ),
            SizedBox(height: 24),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: 3,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    showModalBottomSheet(
                      context: context,
                      builder: (context) => ModalFoodDetail(),
                      backgroundColor: Colors.transparent,
                    );
                  },
                  borderRadius: BorderRadius.circular(defaultBorderRadius),
                  child: Row(
                    children: [
                      Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          color: lightGreyColor,
                          borderRadius:
                              BorderRadius.circular(defaultBorderRadius),
                          image: DecorationImage(
                            image: AssetImage('assets/resto_1.png'),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0, 20),
                              spreadRadius: -15,
                              blurRadius: 12,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 16),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Paket Burger Biasa ${++index}',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                          SizedBox(height: 4),
                          Text(
                            'Lorem ipsum dolor sit amet. Consectetur is simply dummy.',
                            style: TextStyle(color: darkGreyColor),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          SizedBox(height: 16),
                          Text(
                            'Rp 12.000',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: greenColor,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ).width(mediaQuery.size.width - 100 - 48),
                    ],
                  ),
                ).padding(bottom: 24);
              },
            ),
          ],
        ),
      ),
    );
  }
}
