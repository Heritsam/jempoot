import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../../core/themes/theme.dart';
import '../../../widgets/buttons/rounded_button.dart';

class ModalFoodDetail extends StatefulWidget {
  const ModalFoodDetail({Key? key}) : super(key: key);

  @override
  _ModalFoodDetailState createState() => _ModalFoodDetailState();
}

class _ModalFoodDetailState extends State<ModalFoodDetail> {
  int qty = 1;

  void increment() {
    setState(() {
      qty++;
    });
  }

  void decrement() {
    if (qty > 0) {
      setState(() {
        qty--;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Material(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(defaultBorderRadius),
          topRight: Radius.circular(defaultBorderRadius),
        ),
      ),
      color: Colors.white,
      elevation: 4,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.close, color: darkGreyColor),
              ),
            ],
          ),
          SizedBox(height: 2),
          Row(
            children: [
              Container(
                height: 120,
                width: 120,
                decoration: BoxDecoration(
                  color: lightGreyColor,
                  borderRadius: BorderRadius.circular(defaultBorderRadius),
                  image: DecorationImage(
                    image: AssetImage('assets/resto_1.png'),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0, 20),
                      spreadRadius: -15,
                      blurRadius: 12,
                    ),
                  ],
                ),
              ),
              SizedBox(width: 16),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Paket Burger Biasa',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 4),
                  Text(
                    'Lorem ipsum dolor sit amet. Consectetur is simply dummy.',
                    style: TextStyle(color: darkGreyColor),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  SizedBox(height: 16),
                  Row(
                    children: [
                      RoundedButton(
                        onPressed: decrement,
                        label: '-',
                        horizontalPadding: 16,
                        radius: defaultBorderRadius,
                      ),
                      Text('$qty').padding(horizontal: 12),
                      RoundedButton(
                        onPressed: increment,
                        label: '+',
                        horizontalPadding: 16,
                        radius: defaultBorderRadius,
                      ),
                    ],
                  ),
                ],
              ).width(mediaQuery.size.width - 120 - 48),
            ],
          ).padding(horizontal: 16),
          SizedBox(height: 24),
          RoundedButton(
            label: 'Tambahkan',
          ).width(mediaQuery.size.width - 32),
          SizedBox(height: mediaQuery.padding.bottom + 16),
        ],
      ),
    );
  }
}
