import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../core/themes/theme.dart';

class FoodCard extends StatefulWidget {
  final ImageProvider image;
  final Function()? onPressed;

  const FoodCard({
    Key? key,
    required this.image,
    this.onPressed,
  }) : super(key: key);

  @override
  _FoodCardState createState() => _FoodCardState();
}

class _FoodCardState extends State<FoodCard> {
  bool _isPressed = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160,
      width: 140,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(defaultBorderRadius),
        color: mediumGreyColor,
        image: DecorationImage(
          image: widget.image,
          fit: BoxFit.cover,
        ),
      ),
    )
        .ripple()
        .clipRRect(all: defaultBorderRadius)
        .boxShadow(
          color: Colors.black38,
          offset: Offset(0, 15),
          spreadRadius: -13,
          blurRadius: 10,
        )
        .gestures(
          onTapChange: (tapStatus) => setState(() => _isPressed = tapStatus),
          // TODO: ganti widget.onPressed
          onTap: () {
            Navigator.pushNamed(context, '/food-resto');
          },
        )
        .scale(all: _isPressed ? 0.95 : 1.0, animate: true)
        .animate(Duration(milliseconds: 175), Curves.fastLinearToSlowEaseIn);
  }
}
