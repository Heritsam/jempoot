import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';

class SelectLocation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Material(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(defaultBorderRadius),
          topRight: Radius.circular(defaultBorderRadius),
        ),
      ),
      color: Colors.white,
      elevation: 4,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.close, color: darkGreyColor),
              ),
            ],
          ),
          SingleChildScrollView(
            child: Column(
              children: [
                // Container(
                //   height: 180,
                //   width: mediaQuery.size.width,
                //   decoration: BoxDecoration(
                //     color: lightGreyColor,
                //     borderRadius: BorderRadius.circular(defaultBorderRadius),
                //   ),
                //   child: const GoogleMap(
                //     initialCameraPosition: CameraPosition(
                //       target: LatLng(-6.5776978, 106.7792039),
                //       zoom: 14,
                //     ),
                //     myLocationButtonEnabled: false,
                //     scrollGesturesEnabled: false,
                //     zoomGesturesEnabled: false,
                //   ),
                // ),
                // SizedBox(height: 24),
                Row(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextFormField(
                          initialValue: 'Jl. Cibereum Petir no. 25, Bogor',
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 16),
                          ),
                          style: TextStyle(color: darkGreyColor, fontSize: 14),
                        ).expanded(),
                        InkResponse(
                          onTap: () {},
                          radius: 24,
                          child: Icon(Icons.close, size: 18),
                        ).padding(horizontal: 12),
                      ],
                    )
                        .decorated(
                          color: lightGreyColor,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(defaultBorderRadius),
                            bottomLeft: Radius.circular(defaultBorderRadius),
                          ),
                        )
                        .expanded(),
                    Icon(Icons.check, color: Colors.white)
                        .padding(all: 12)
                        .ripple()
                        .backgroundColor(orangeColor)
                        .clipRRect(
                          topRight: defaultBorderRadius,
                          bottomRight: defaultBorderRadius,
                        )
                        .gestures(),
                  ],
                ).padding(horizontal: 16),
                SizedBox(height: 24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Alamat Tersimpan',
                        style: TextStyle(color: darkGreyColor)),
                    InkResponse(
                      onTap: () {},
                      child: Text(
                        'Tambah Alamat',
                        style: TextStyle(color: orangeColor, fontSize: 12),
                      ),
                    ),
                  ],
                ).padding(horizontal: 16),
                SizedBox(height: 16),
                ListView.builder(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemCount: 6,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(right: 8),
                      child: Chip(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        label: Text(
                          'Rumah',
                          style: TextStyle(color: Colors.white),
                        ),
                        backgroundColor: orangeColor,
                      ),
                    );
                  },
                ).height(32),
                SizedBox(height: 24),
                Divider(color: mediumGreyColor),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  itemCount: 6,
                  itemBuilder: (context, index) {
                    return ListTile(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      title: Text('Jl. Cibereum Petir'),
                      subtitle: Text(
                        'Kp. Cibeureum Petir 25, Blok IV, No. 12 RT. 12/21',
                      ),
                    );
                  },
                ),
                SizedBox(height: mediaQuery.padding.bottom + 16),
              ],
            ),
          ).expanded(),
        ],
      ),
    ).padding(top: mediaQuery.padding.top + 32);
  }
}
