import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';

class SmallChip extends StatelessWidget {
  final Widget child;
  final Color backgroundColor;
  final double borderRadius;

  const SmallChip({
    Key? key,
    required this.child,
    this.backgroundColor = lightGreyColor,
    this.borderRadius = 50,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return child.padding(horizontal: 8, vertical: 4).decorated(
          color: backgroundColor,
          borderRadius: BorderRadius.circular(borderRadius),
        );
  }
}
