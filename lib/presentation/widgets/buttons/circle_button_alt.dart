import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';
import 'circle_button.dart';

class CircleButtonAlt extends StatelessWidget {
  final Widget icon;
  final String label;
  final double size;
  final bool disabled;
  final Function()? onPressed;

  const CircleButtonAlt({
    Key? key,
    required this.icon,
    required this.label,
    this.size = 50,
    this.disabled = false,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Column(
      children: [
        CircleButton(
          onPressed: onPressed,
          child: icon,
          backgroundColor: Colors.white,
          padding: 10,
        ),
        SizedBox(height: 8),
        Text(
          label,
          style: TextStyle(
            fontWeight: FontWeight.w500,
            color: disabled ? darkGreyColor : orangeColor,
          ),
          textAlign: TextAlign.center,
        ).constrained(width: mediaQuery.size.width / 4 - 24),
      ],
    );
  }
}
