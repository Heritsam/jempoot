import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

import '../../../core/themes/theme.dart';

class JempootCardAlt extends StatefulWidget {
  final Widget child;
  final Color backgroundColor;
  final EdgeInsets? padding;
  final BoxDecoration? decoration;
  final Function()? onTap;

  const JempootCardAlt({
    Key? key,
    required this.child,
    this.backgroundColor = Colors.white,
    this.padding,
    this.decoration,
    this.onTap,
  }) : super(key: key);

  @override
  _JempootCardAltState createState() => _JempootCardAltState();
}

class _JempootCardAltState extends State<JempootCardAlt> {
  bool _isPressed = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: widget.child,
      padding: widget.padding,
      decoration: widget.decoration,
    )
        .borderRadius(all: defaultBorderRadius)
        .ripple()
        .backgroundColor(widget.backgroundColor)
        .clipRRect(all: defaultBorderRadius)
        .borderRadius(all: defaultBorderRadius, animate: true)
        .boxShadow(
          color: Colors.black12,
          offset: Offset(0, 20),
          spreadRadius: -15,
          blurRadius: 12,
        )
        .gestures(
          onTapChange: (tapStatus) => setState(() => _isPressed = tapStatus),
          onTap: widget.onTap,
        )
        .scale(all: _isPressed ? 0.95 : 1.0, animate: true)
        .animate(Duration(milliseconds: 175), Curves.fastLinearToSlowEaseIn);
  }
}
