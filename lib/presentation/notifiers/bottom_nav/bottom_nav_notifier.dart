import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final bottomNavNotifier = ChangeNotifierProvider<BottomNavNotifier>((ref) {
  return BottomNavNotifier();
});

class BottomNavNotifier extends ChangeNotifier {
  BottomNavNotifier() {
    controller = PageController(initialPage: pageIndex);
  }
  
  late PageController controller;
  int pageIndex = 0;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void changeIndex(int index) {
    pageIndex = index;
    controller.jumpToPage(index);
    notifyListeners();
  }
}