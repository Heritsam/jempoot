import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/themes/theme.dart';
import 'presentation/pages/contact/contact_page.dart';
import 'presentation/pages/faq/faq_page.dart';
import 'presentation/pages/favorite/favorite_food_page.dart';
import 'presentation/pages/food/food_list_page.dart';
import 'presentation/pages/food/food_page.dart';
import 'presentation/pages/food_checkout/food_checkout_page.dart';
import 'presentation/pages/food_resto/food_resto_page.dart';
import 'presentation/pages/history_food/history_food_detail_page.dart';
import 'presentation/pages/history_food/history_food_page.dart';
import 'presentation/pages/history_jempoot/history_jempoot_detail_page.dart';
import 'presentation/pages/history_jempoot/history_jempoot_page.dart';
import 'presentation/pages/history_kirim/history_kirim_detail_page.dart';
import 'presentation/pages/history_kirim/history_kirim_page.dart';
import 'presentation/pages/login/login_page.dart';
import 'presentation/pages/main_page.dart';
import 'presentation/pages/notification/notification_detail_page.dart';
import 'presentation/pages/notification/notification_page.dart';
import 'presentation/pages/otp/otp_page.dart';
import 'presentation/pages/profile/profile_edit_page.dart';
import 'providers.dart';
import 'wrapper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final preferences = await SharedPreferences.getInstance();

  runApp(ProviderScope(
    overrides: [
      sharedPreferences.overrideWithValue(preferences),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Jempoot',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        primaryColor: orangeColor,
        accentColor: orangeColor,
        fontFamily: 'SF Pro',
        snackBarTheme: SnackBarThemeData(behavior: SnackBarBehavior.floating),
        scaffoldBackgroundColor: Colors.white,
        appBarTheme: AppBarTheme(
          elevation: 8,
          shadowColor: Colors.black45,
        )
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => Wrapper(),
        '/home': (context) => MainPage(),
        '/login': (context) => LoginPage(),
        '/otp': (context) => OtpPage(),
        '/edit-profile': (context) => ProfileEditPage(),
        '/faq': (context) => FaqPage(),
        '/contact': (context) => ContactPage(),
        '/notification': (context) => NotificationPage(),
        '/notification-detail': (context) => NotificationDetailPage(),
        '/history-food': (context) => HistoryFoodPage(),
        '/history-food-detail': (context) => HistoryFoodDetailPage(),
        '/history-jempoot': (context) => HistoryJempootPage(),
        '/history-jempoot-detail': (context) => HistoryJempootDetailPage(),
        '/history-kirim': (context) => HistoryKirimPage(),
        '/history-kirim-detail': (context) => HistoryKirimDetailPage(),
        '/favorite': (context) => FavoriteFoodPage(),
        '/food': (context) => FoodPage(),
        '/food-list': (context) => FoodListPage(),
        '/food-resto': (context) => FoodRestoPage(),
        '/food-checkout': (context) => FoodCheckoutPage(),
      },
    );
  }
}
